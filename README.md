# irstea/phpcpd-shim - shim repository for sebastian/phpcpd.

This package is a drop-in replacement for [sebastian/phpcpd](https://github.com/sebastianbergmann/phpcpd), which provides its PHAR archive as a binary.

It is built automatically from the official PHAR.

## Installation

	composer require irstea/phpcpd-shim

or:

	composer require --dev irstea/phpcpd-shim



## Usage

As you would use the original package, i.e. something like:

	vendor/bin/phpcpd [options] [arguments]

## License

This distribution retains the license of the original software: BSD-3-Clause